(when (maybe-require-package 'yasnippet)  
  (yas-global-mode t)
  
  ;; Disable yasnippet mode on some mode.
  (dolist (hook (list
		 'term-mode-hook
		 ))
    (add-hook hook '(lambda () (yas-minor-mode -1))))

  )

(provide 'init-yasnippet)
