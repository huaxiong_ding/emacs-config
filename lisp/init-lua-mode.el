;; indent 2 spaces by default
(when (maybe-require-package 'lua-mode)
  (require 'lua-mode)
  (setq-default lua-indent-level 2))

(provide 'init-lua-mode)
