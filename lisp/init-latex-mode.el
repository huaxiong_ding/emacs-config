(setq latex-run-command "xelatex")


(add-hook 'text-mode-hook
	  (lambda()
	    (set-fill-column 120)
	    'turn-on-auto-fill
	    ))

(provide 'init-latex-mode)
