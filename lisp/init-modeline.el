(setq-default mode-line-format
    (list
    ;; the buffer name
    "[%b](%l,%01c)"

    ;; the current major mode for the buffer.
    "[%m "

    ;; insert vs overwrite mode, input-method in a tooltip
    '(:eval (propertize (if overwrite-mode "Ovr" "Ins")
              'face nil
              'help-echo (concat "Buffer is in "
                           (if overwrite-mode "overwrite" "insert") " mode")))

    ;; was this buffer modified since the last save?
    '(:eval (when (buffer-modified-p)
              (concat ","  (propertize "Mod"
                             'face nil
                             'help-echo "Buffer has been modified"))))

    ;; is this buffer read-only?
    '(:eval (when buffer-read-only
              (concat ","  (propertize "RO"
                             'face nil
                             'help-echo "Buffer is read-only"))))
    "]"

    "["
    (propertize "%M" 'face nil)
    "] "

    "%-"
    )
  )

(setq display-time-default-load-average nil)
(display-time-mode 1)
(provide 'init-modeline)
