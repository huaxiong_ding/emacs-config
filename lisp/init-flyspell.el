(maybe-require-package 'flycheck)
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'prog-mode-hook 'flyspell-prog-mode)
(provide 'init-flyspell)
