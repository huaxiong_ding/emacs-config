(maybe-require-package 'langtool)

(setq langtool-language-tool-jar "/home/ding/Apps/LanguageTool-3.5/languagetool-commandline.jar")

(global-set-key "\C-x4w" 'langtool-check)
(global-set-key "\C-x4W" 'langtool-check-done)
(global-set-key "\C-x4l" 'langtool-switch-default-language)
(global-set-key "\C-x44" 'langtool-show-message-at-point)
(global-set-key "\C-x4c" 'langtool-correct-buffer)


(setq langtool-default-language "en-US")

(provide 'init-langtool)
