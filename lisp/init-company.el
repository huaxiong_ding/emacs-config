(maybe-require-package 'company)

(add-hook 'after-init-hook 'global-company-mode)

(setq company-dabbrev-downcase nil)
(setq company-dabbrev-ignore-case nil)
(setq company-show-numbers t)
(setq company-idle-delay 0.2)
(setq company-clang-insert-arguments nil)
(setq company-require-match nil)
(setq company-etags-ignore-case t)
(setq company-auto-complete nil)

(defun sanityinc/local-push-company-backend (backend)
  "Add BACKEND to a buffer-local version of `company-backends'."
  (set (make-local-variable 'company-backends)
       (append (list backend) company-backends)))


(provide 'init-company)
