(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(require 'init-elpa)
(require 'init-utils)
(require 'init-modeline)
(require 'init-gui-frames)
(require 'init-color-theme)

(require 'init-flyspell)

(require 'init-ido)
(require 'init-smex)
(require 'init-dired)
(require 'init-uniquify)
(require 'init-ibuffer)

(require 'init-flycheck)
(require 'init-hippie-expand)

(require 'init-yasnippet)
(require 'init-company)

(require 'init-lua-mode)
(require 'init-python-mode)
(require 'init-bash-mode)
(require 'init-docker-mode)
(require 'init-cmake-mode)
(require 'init-latex-mode)
(require 'init-langtool)
